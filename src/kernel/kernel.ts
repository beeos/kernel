import { IDisplay, IKeyboard } from "./devices";
import { IEstablishStdConnection, IProgram, IProcess, KernelCall, kernel_call } from "./program";
import { Shell } from "../programs/shell";
import * as BrowserFS from 'browserfs'
import InMemoryFileSystem from "browserfs/dist/node/backend/InMemory";

export class Kernel {
  private static displays: IDisplay[] = [];
  private static keyboard: IKeyboard;
  private static processes: IProgram[] = [];
  private static BFS: any = {};
  private static keyboardListeners: ((e: KeyboardEvent) => void)[] = [];
  private static keysDown: {[keyCode: string]: boolean} = {};
  private static fs: any;

  public static async main() {
    const display = document.querySelector<HTMLCanvasElement>("#screen1");
    if (display) this.addDisplay(display);
    await this.setupFilesystem();
    this.setupKeyboard();
    await this.install();
    this.init();
  }

  private static setupKeyboard() {
    document.addEventListener('keydown', (e) => {
      this.keysDown[e.code] = true;
      this.dispatchKeyboardEvent(e);
    });
    document.addEventListener('keyup', (e) => {
      this.keysDown[e.code] = false;
      this.dispatchKeyboardEvent(e);
    });
  }

  private static addKeyboardListener(listener: (e: KeyboardEvent) => void) {
    this.keyboardListeners.push(listener);
  }

  private static dispatchKeyboardEvent(e: KeyboardEvent) {
    this.keyboardListeners.forEach(listener => listener(e));
  }

  private static async setupFilesystem() {
    return new Promise((resolve, reject) => {
      BrowserFS.install(this.BFS);
      BrowserFS.FileSystem.InMemory.Create(function(e: BrowserFS.Errors.ApiError, inMemory: InMemoryFileSystem) {
        BrowserFS.FileSystem.LocalStorage.Create({}, function(e, lsfs) {
          BrowserFS.FileSystem.MountableFileSystem.Create({
            '/tmp': inMemory,
            '/': lsfs!
          }, function(e, mfs) {
            BrowserFS.initialize(mfs!);
            resolve();
          });
        });
      }, () => {});
      this.fs = this.BFS.require('fs');
    })
  }

  private static async init() {
    const result = await this.executeProgram(new Shell());
    console.log(result);
  }

  private static install() {
    const dirs = [
      "/bin"
    ];
    const files = [
      ["/bin/ls", "/bin/ls.js"],
      ["/bin/cat", "/bin/cat.js"],
      ["/bin/echo", "/bin/echo.js"],
      ["/bin/owo", "/bin/owo.js"],
      ["/bin/mkdir", "/bin/mkdir.js"],
    ];

    for (let dir of dirs) {
      try {
        this.fs.mkdirSync(dir);
      }
      catch (err) {
        
      }
    }

    return new Promise(async (resolve) => {
      const loadFile = async (fileIndex = 0) => {
        const f = files[fileIndex];
        if (!f) return;
        const contents = await (await fetch(f[1])).text();
        this.fs.writeFileSync(f[0], contents);
        loadFile(fileIndex + 1);
      }
      await loadFile();
      resolve();
    });
  }

  private static addDisplay(canvas: HTMLCanvasElement) {
    let id = 0;
    if (this.displays[this.displays.length - 1])
        id = this.displays[this.displays.length - 1].id + 1;
    this.displays.push({id, canvas, context: canvas.getContext('2d')!});
  }

  private static executeProgram(code: string | IProgram, connection?: IEstablishStdConnection, args?: string[]) {
    let program: IProgram;
    if ((<string> code).trim) {
      const f = new Function("execute", "exports", <string> code);
      let process;
      let exports = {};
      f((main: (call: KernelCall, connection?: IEstablishStdConnection, args?: string[]) => Promise<IProcess>) => process = main((call, ...args) => this.call((<kernel_call> call), ...args), connection, args), exports);
      console.log("PROCESS: ", process);
      return process;
    }
    else {
      program = (<IProgram> code);
      const process = program.run((call, ...args) => this.call((<kernel_call> call), ...args), connection, args);
      const l = this.processes.push(program);
      program.pid = this.processes[l - 1].pid + 1;
      process.then(() => {
        delete this.processes.filter(prog => prog.pid === program.pid)[0];
      });
      return process;
    }
  }

  private static getDisplayById(id: number) {
    return this.displays.filter(d => d.id === id)[0];
  }

  public static call(code: kernel_call, ...args: any[]) {
    console.log("KERNEL CALL: ", code);
    switch (code) {
      case 'draw-to-screen':
        return this.drawToScreen(args);
      case 'fs-read-file':
        return this.fsReadFile(args);
      case 'fs-read-dir':
        return this.fsReadDir(args);
      case 'fs-make-directory':
        return this.fsMakeDirectory(args);
      case 'fs-exists':
        return this.fsExists(args);
      case 'execute-program':
        return this.executeProgram(args[0], args[1], args[2]);
      case 'add-keyboard-listener':
        return this.addKeyboardListener(args[0]);
      case 'get-keyboard-state':
        return this.keysDown;
      case 'network-http':
        return this.networkHttp(args[0], args[1]);
      default:
        throw new Error("Bah");
    }
  }

  private static networkHttp(url = ``, options = {}) {
    const defaults = {
      method: "GET",
    }
    return fetch(url, Object.assign(defaults, options));
  }

  private static fsReadDir(args: any[]) {
    // args: [directory]
    return this.fs.readdirSync(args[0]);
  }

  private static fsReadFile(args: any[]) {
    // args: []
    return this.fs.readFileSync(args[0]);
  }

  private static fsMakeDirectory(args: any[]) {
    return this.fs.mkdirSync(args[0]);
  }

  private static fsExists(args: any[]) {
    return this.fs.existsSync(args[0]);
  }

  private static drawToScreen(args: any[]) {
    const [display, func] = args;
    const ctx = this.getDisplayById(display).context;
    func(ctx);
  }
}