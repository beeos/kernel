export type KernelCall = (code: kernel_call, ...args: any) => any;

export type kernel_call = 
'draw-to-screen' |
'fs-read-file' |
'fs-read-dir' |
'fs-make-directory' |
'fs-exists' |
'execute-program' |
'add-keyboard-listener' |
'get-keyboard-state' |
'network-http';

export interface ISchedule {
  callback: () => void;
  time: number;
}

export interface IEstablishStdConnection {
  establishStdIn: (stdIn: Pipe) => void;
  establishStdOut: () => Pipe;
}

export interface IProgram {
  pid: number;
  run(call: KernelCall, connection?: IEstablishStdConnection, args?: string[]): Promise<IProcess>;
  pipes?: IStdIO;
}

export type Pipe = (message: string) => void;

export interface IProcess {
  exitCode: number;
}

export interface IStdIO {
  in: Pipe[];
  out: Pipe[];
}
