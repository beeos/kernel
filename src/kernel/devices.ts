export interface IDisplay {
  id: number,
  canvas: HTMLCanvasElement;
  context: CanvasRenderingContext2D;
}

export interface IKeyboard {
  listen(event: string, callback: () => void): void;
}
