import { IProcess, IProgram, Pipe, IStdIO, KernelCall, IEstablishStdConnection } from "../kernel/program";
import * as path from 'path';

export class Shell implements IProgram {
  pipes: IStdIO = {
    in: [],
    out: []
  };

  pid = 0;
  prompt = () => this.cwd + ' # ';
  cwd = '/';
  buffer = 'Welcome to BeeOS!\n' + this.prompt();
  fontSize = 12;
  showCaret = true;
  caretCycle = 1;
  inputBuffer = '';
  caretLocation = 0;
  path = '/bin';
  history: string[] = [];
  historyPlaceholder = "";

  englishLetters = new Map([
    ["KeyA", "A"],
    ["KeyB", "B"],
    ["KeyC", "C"],
    ["KeyD", "D"],
    ["KeyE", "E"],
    ["KeyF", "F"],
    ["KeyG", "G"],
    ["KeyH", "H"],
    ["KeyI", "I"],
    ["KeyJ", "J"],
    ["KeyK", "K"],
    ["KeyL", "L"],
    ["KeyM", "M"],
    ["KeyN", "N"],
    ["KeyO", "O"],
    ["KeyP", "P"],
    ["KeyQ", "Q"],
    ["KeyR", "R"],
    ["KeyS", "S"],
    ["KeyT", "T"],
    ["KeyU", "U"],
    ["KeyV", "V"],
    ["KeyW", "W"],
    ["KeyX", "X"],
    ["KeyY", "Y"],
    ["KeyZ", "Z"],
  ]);

  specialCharacters = new Map([
    ["Space", [" ", " "]],
    ["Backquote", ["`", "~"]],
    ["Digit1", ["1", "!"]],
    ["Digit2", ["2", "@"]],
    ["Digit3", ["3", "#"]],
    ["Digit4", ["4", "$"]],
    ["Digit5", ["5", "%"]],
    ["Digit6", ["6", "^"]],
    ["Digit7", ["7", "&"]],
    ["Digit8", ["8", "*"]],
    ["Digit9", ["9", "("]],
    ["Digit0", ["0", ")"]],
    ["Slash", ["/", "?"]],
    ["Period", [".", ">"]],
    ["Comma", [",", "<"]],
    ["Minus", ["-", "_"]],
    ["Equal", ["=", "+"]],
    ["BracketLeft", ["[", "{"]],
    ["BracketRight", ["]", "}"]],
    ["Backslash", ["\\", "|"]],
    ["Semicolon", [";", ":"]],
    ["Quote", ["'", "\""]],
    // ["Enter", ["\n", "\n"]]
  ]);

  builtinCommands: {[key: string]: IProgram} = {
    "cd": {
      pid: 0,
      run: (call: KernelCall, connection: IEstablishStdConnection, args: string[]) => new Promise((resolve) => {
        const newDir = path.resolve(args[0], args[1]);
        const exists = call('fs-exists', newDir);
        if (exists) {
          this.cwd = newDir;
          resolve({
            exitCode: 0
          });
        }
        else {
          resolve({
            exitCode: 1
          });
        }
      })
    },
    "clear": {
      pid: 0,
      run: (call: KernelCall, connection: IEstablishStdConnection, args: string[]) => new Promise((resolve) => {
        this.buffer = "";
        resolve({
          exitCode: 0
        });
      })
    },
    "ping": {
      pid: 0,
      run: async (call: KernelCall, connection: IEstablishStdConnection, args: string[]) => new Promise<IProcess>(async (resolve) => {
        const out = connection.establishStdOut();
        try {
          await call('network-http', args[1], {mode: 'no-cors'});
          out("Success\n");
        }
        catch {
          out("Fail\n");
        }
        resolve({
          exitCode: 0
        });
      })
    }
  }

  public registerOutPipe(pipe: Pipe) {
    this.pipes.out.push(pipe);
  }

  private sendMessageToStdOut(message: string) {
    this.pipes.out.forEach(pipe => pipe(message));
  }

  render(call: KernelCall) {
    call('draw-to-screen', 0, (ctx: CanvasRenderingContext2D) => {
      ctx.fillStyle = 'black';
      ctx.rect(0, 0, ctx.canvas.width, ctx.canvas.height);
      ctx.fill();
      ctx.font = `${this.fontSize}px "Inconsolata", monospace`;
      ctx.fillStyle = 'white';
      const lines = this.buffer.split('\n');
      for (let line = 0; line < lines.length; line++) {
        let text = lines[line];
        if (line + 1 === lines.length && this.showCaret && this.caretCycle) {
          let bufArray = this.inputBuffer.split("");
          bufArray[this.caretLocation] = '█';
          text += bufArray.join("");
        }
        ctx.fillText(text, 10 + this.fontSize, 10 + (this.fontSize + 10) * (line + 1), ctx.canvas.width - 10);
      }
    })
  }

  ex(call: KernelCall, program: string | IProgram, args: string[]) {
    this.showCaret = false;
    const connection: IEstablishStdConnection = {
      establishStdIn: (stdIn: Pipe) => {
        console.log(stdIn);
      },
      establishStdOut: () => {
        return (message) => {
          this.buffer += message;
          this.render(call);
        }
      }
    };
    const process = call('execute-program', program, connection, args);
    process.then(() => {
      this.setPrompt();
      this.showCaret = true;
      this.render(call);
    })
  }

  private setPrompt() {
    this.buffer += this.prompt();
  }

  private commitInput() {
    this.buffer += this.inputBuffer + "\n";
    this.caretLocation = 0;
    this.history.push(this.inputBuffer);
    this.inputBuffer = '';
  }

  private addToBuffer(char: string) {
    const start = this.inputBuffer.substring(0, this.caretLocation);
    const end = this.inputBuffer.substring(this.caretLocation, this.inputBuffer.length);
    this.inputBuffer = start + char + end;
    this.caretLocation++;
  }

  private timeBack() {
    
  }

  run(call: KernelCall) {
    return new Promise<IProcess>((resolve, reject) => {
      this.render(call);
      call('add-keyboard-listener', (e: KeyboardEvent) => {
        if (!call('get-keyboard-state')[e.code]) return;
        let char = this.englishLetters.get(e.code);
        const shiftIsPressed = call('get-keyboard-state').ShiftLeft || call('get-keyboard-state').ShiftRight;
        if (char) {
          char = char.toLocaleLowerCase();
          if (shiftIsPressed) {
            char = char.toLocaleUpperCase();
          }
          this.addToBuffer(char);
        }
        let specialChar = this.specialCharacters.get(e.code);
        if (specialChar) {
          if (shiftIsPressed) char = specialChar[1];
          else char = specialChar[0];
          this.addToBuffer(char);
        }
        if (e.code === "Enter") {
          if (this.showCaret) {
            if (this.inputBuffer) {
              const words = this.inputBuffer.split(' ');
              const executableName = words.shift();
              let dir = call('fs-read-dir', this.cwd);
              if (executableName) {
                if (this.builtinCommands[executableName]){
                  console.log("Running built-in command.");
                  this.commitInput();
                  this.ex(call, this.builtinCommands[executableName], [this.cwd, ...words]);
                }
                else if (dir.indexOf(executableName) !== -1) {
                  this.commitInput();
                  console.log("Running from current dir.");
                }
                else {
                  dir = call('fs-read-dir', this.path);
                  if (dir.indexOf(executableName) !== -1) {
                    console.log("Running from path dir.");
                    this.commitInput();
                    try {
                      const file = call('fs-read-file', this.path + '/' + executableName);
                      const code = file.toString();
                      this.ex(call, code, [this.cwd, ...words]);
                    }
                    catch (err) {
                      console.log(err);
                      this.setPrompt();
                    }
                  }
                  else {
                    this.commitInput();
                    console.log(`Could not find executable: ${executableName}`);
                    this.setPrompt();
                    this.render(call);
                  }
                }
              }
            }
            else {
              this.inputBuffer += " ";
              this.commitInput();
              this.setPrompt();
              this.render(call);
            }
          }
        }
        if (e.code === "Backspace") {
          if (this.caretLocation > 0) {
            const buff = this.inputBuffer.split('');
            buff.splice(this.caretLocation - 1, 1);
            this.inputBuffer = buff.join('');
            this.caretLocation--;
          }
        }
        if (e.code === 'ArrowLeft') {
          if (this.caretLocation > 0) this.caretLocation--;
        }
        if (e.code === 'ArrowRight') {
          if (this.caretLocation < this.inputBuffer.length) this.caretLocation++;
        }
        this.render(call);
      });
      // this.ex(call, new Cat(), ['/bin/ls']);
    });
  }
}