const gulp = require('gulp');

gulp.task('copy:www', () => {
  gulp.src('./www/**/*.*')
    .pipe(gulp.dest('./dist'));
});

gulp.task('default', ['copy:www']);
